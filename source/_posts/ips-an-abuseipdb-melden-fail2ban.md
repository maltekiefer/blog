---
title: IP's an AbuseIPDB melden (Fail2Ban)
date: 2020-07-02 15:00:00
tags:
- debian
- server
- linux
- abuseipdb
- fail2ban
- ufw
---
In meinem letzten Artikel habe ich erklärt, wie man unter [Linux die Firewall mit einem IP Block ausstatten kann](https://blog.beli3ver.de/2020/07/01/linux-firewall-mit-blacklisten-ip-block/).
In diesem Artikel möchte ich euch erklären, wie ihr auch IP's melden könnt, und somit der Community wieder was zurückgeben könnt.

## Fail2Ban installieren

Fail2Ban ist schnell installiert:

```bash
apt install -y fail2ban
```

## Fail2Ban und UFW

Wie im ersten Artikel bereits beschrieben, nutze ich als Firewall für meine Systeme UFW. Damit Fail2Ban mit UFW sprechen kann, müssen wir kleine Anpassung machen.
Als Erstes müssen wir die Datei `jail.local` erstellen, dazu macht einfach folgendes:

```bash
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```
In der `jail.local` such jetzt bitte die folgende Zeile (ist bei mir die 167):

```bash
banaction = ....
```
Alles hinter dem Gleichzeichen entfernen und `ufw` angeben.

## Fail2Ban und AbuseIPDB

Als Nächstes müssen wir in der `jail.local` noch eine Änderung vornehmen.
Sucht in der Datei, im `[DEFAULT]` Block bitte die folgende Zeile (ist bei mir 228 kurz vor dem sshd Jail):

```bash
action = %(action_)s
```
Diese muss durch die folgende erstes werden, passt auch direkt deine API Key an.

```bash
action = %(action_)s
         %(action_abuseipdb)s[abuseipdb_apikey="DeinAPIKey", abuseipdb_category="18"]
```
Was haben wir gerade gemacht?
Wir haben Fail2Ban gesagt, dass er bei einem Ban, die IP die gebannt wird, an die Aktion AbuseIPDB (bearbeiten wir gleich) übergeben werden soll, mit der Kategorie 18 (Brute-Force).
Weiteres zu den Kategorien [hier](https://www.abuseipdb.com/categories).

In diesem Teil habe ich jetzt beschrieben, wie wir allgemein eine Aktion für alle Bans aufrufen können. Natürlich kann man das für jeden Jail auch einzelnen machen, und so genauer melden, wenn man die entsprechenden Kategorien angibt.
So könnte man die Zeile oben auf Default lassen und im Jail `sshd` zum Beispiel das folgende machen:

```bash
[sshd]
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
action = %(action_)s
         %(action_abuseipdb)s[abuseipdb_apikey="DeinAPIKey", abuseipdb_category="18,22"]
```
Jetzt sind die Kategorien die wir melden, 18 (Brute-Force) und 22 (SSH). So kann AbuseIPDB, das viel genauer filtern und anderen zur Verfügung stellen.

Als letztes müssen wir noch die Action abuseipdb anpassen. Leider, ist die action die mit Debian mitgeliefert wird nicht mehr aktuell, und führt dazu, das ihr nichts melden könnt, weil die API falsch angesprochen wird.
Dazu geht in die Datei `/etc/fail2ban/action.d/abuseipdb.conf`.

Löscht die aktuelle Zeile, die mit `actionban =` begint komplett und setzt die folgenden rein:

```bash
actionban = curl --tlsv1.2 --fail 'https://api.abuseipdb.com/api/v2/report' -H 'Accept: application/json' -H 'Key: <abuseipdb_apikey>' --data-urlencode "comment=<matches>" --data-urlencode 'ip=<ip>' --data 'categories=<abuseipdversionb_category>'
```
**WICHTIG: hier nichts einsetzen, so eintragen wie ich es hier geschrieben habe, der APIKey und die IP und die Kategorien werden von Fail2Ban bei einem Ban übergeben!**

Jetzt alles speichern und einen neustart von fail2ban machen:

```bash
fail2ban-client restart
```
Hier sollte kein Fehler kommen, sicherheitshalber kann man auch nochmal die Log überprüfen:

```bash
cat /var/log/fail2ban.log
```

## Ergebnis
Dementsprechend wie sehr eurer Server "angegriffen" wird, solltet ihr nach einigen Tagen bereits einige IP's gemeldet haben, bei mir sieht das aktuell so aus:

<a href="https://www.abuseipdb.com/user/38312" title="AbuseIPDB is an IP address blacklist for webmasters and sysadmins to report IP addresses engaging in abusive behavior on their networks" alt="AbuseIPDB Contributor Badge">
	<img src="https://www.abuseipdb.com/contributor/38312.svg" style="width: 200px;">
</a>
