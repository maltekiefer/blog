---
title: Linux Firewall mit Blacklisten (IP Block)
date: 2020-07-01 15:00:00
tags:
- debian
- server
- linux
- abuseipdb
- fail2ban
- ufw
---
Im folgenden Artikel möchte ich erläutern, wie ihr eure Linux Firewall (UFW) mit einer Blackliste von IP-Adressen, die durch Meldungen bereits als schadhafte IP's (wurden zum Beispiel für Brute-Force) genutzt, erweitern könnt.

## AbuseIPDB

Ich nutze für dieses Setup die Datenbank von [AbuseIPDB](https://www.abuseipdb.com/). Damit auch ihr diese nutzen könnt, müsst ihr euch einfach einen kostenlosen Account erstellen.
Danach könnt ihr in den Einstellungen einen API Key generieren, den ihr für das folgenden Setup benötigt.

## Software installieren

Wir benötigen natürlich einige Softwarepakete. Grundsätzlich, gehe ich davon aus, das ihr UFW bereits installiert habt und entsprechend konfiguriert habt.
Zusätzliche Pakete, die noch benötigt werden:

```bash
apt install -y screen curl
```

## Ordnerstruktur

Jetzt legen wir den Ordner an, den wir benötigen:

```bash
mkdir /opt/blacklist
```

## Los geht es

Ok, jetzt fangen wir dann mal wirklich an. Die erste Datei, die wir anlegen heißt `blacklist.sh`und liegt in dem eben von uns erstellten Ordner. Der Inhalt ist klein und fein und sieht so aus:

```bash
#!/bin/bash

while read line;
do
	ufw insert 1 deny from $line to any;
done < /opt/blacklist/blacklist
```

Das Script macht nichts anderes, als die Datei `/opt/blacklist/blacklist` Zeile für Zeile einzulesen und entsprechend an die UFW Firewall zu übergeben und zu blockieren. Richtig, jede Zeile enthält eine potenzielle "böse" IP-Adresse.

Entsprechend setzten wir jetzt noch die Rechte auf die Datei:

```bash
chmod 700 /opt/blacklist/blacklist.sh
```

Um diese Liste mit den IP-Adressen zu erstellen, benötigen wir noch ein zweites Script, diese legen wir direkt im Verzeichnis `/etc/cron.daily/` an. (Achtung, bei schwachen Servern sollte eher in diesem Verzeichnis `/etc/cron.weekly/` die Datei angelegt werden). Das Script hat den Namen `getBlacklist`. Der Inhalt der Datei, sieht wie folgt aus:

```bash
#!/bin/bash

#get latest blacklist from abuseIPDB

curl -G https://api.abuseipdb.com/api/v2/blacklist \
  -d confidenceMinimum=50 \
  -H "Key: EurerAPIKey" \
  -H "Accept: text/plain" | sort > /opt/blacklist/blacklist

#block every ip in list

/usr/bin/bash /opt/blacklist/blacklist.sh
```
**Bitte ersetzt EurerAPIKey mit eurem Key, achtet auf das Leerzeichen nach dem Doppelpunkt!**

Entsprechend setzten wir jetzt noch die Rechte auf die Datei:

```bash
chmod 755 /etc/cron.daily/getBlacklist
```

Diese Datei holt die IP-Adressen aus der *AbuseIPDB* Datenbank und schreibt diese in die *blacklist* Datei. Danach wird unser erstes Script aufgerufen.
Der Wert confidenceMinimum gibt das folgenden an:
```
Wir empfehlen Ihnen, nach abuseConfidenceScore zu filtern, d.h. nach unserer berechneten Bewertung, wie missbräuchlich die IP ist, basierend auf den Nutzern, die sie gemeldet haben (mehr).
```
Quelle: [AbuseIPDB Dokumentation](https://docs.abuseipdb.com/#blacklist-endpoint)

## Testen

**Wichtig: Diese Liste enthält aktuell über 10.000 IP-Adressen**

Das Einfügen einer solchen Menge in die Firewall, dauert.
Also werden wir den folgenden Befehl ausführen, nachdem wir `screen` gestartet haben.

```bash
screen
# Enter drücken
# dann folgendes ausführen
bash /etc/cron.daily/getBlacklist
```

Jetzt sollten die IP's geladen werden und dann alles in die Firewall eingetragen werden.
Mit Strg + a und Strg + d verlasst ihr die aktuelle screen Session.
Um das Ergebnis zu sehen, gebt einfach folgenden Befehl ein:

```bash
ufw status
```

Das Bild sollte dann diesem hier sehr ähnlich sein:
![](https://i.snap.as/o8cwMbq.png)
