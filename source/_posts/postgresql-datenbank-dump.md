---
title: PostgreSQL Datenbank Dump
date: 2020-07-01 14:00:00
tags:
- debian 
- server
- linux
- databse
- postgresql
---
In meinem neuen Unternehmen arbeiten wir nur noch mit PostgreSQL Datenbanken. Für ein aktuelles Projekt haben wir jetzt in die Backupstrategie natürlich auch den Dump der jeweiligen Datenbank mit aufgenommen. Im Folgenden will ich kurz erklären, wie es schnell unter Linux mit Cron zu lösen ist.

Dazu legen wir eine Datei an, mit dem Name `db_backup.sh`.
Diese sollte dann die folgenden Rechte gesetzt bekommen:

```bash
chmod 700 db_backup.sh
```
Grund hierfür ist, dass wir in die Datei, das Passwort des jeweiligen DB-Nutzers ablegen müssen.

Als Nächstes kommen wir zum Inhalt:

```bash
#!/bin/bash

date=$(date +%Y-%m-%d-%H-%M)
export PGPASSWORD=PasswortVomNutzerTest
/usr/bin/pg_dump -C -f /opt/backup/dumps/${date}_postgres_dump.sql --encoding=UTF-8 -U Test

find /opt/backup/dumps -mtime +5 -exec rm {} \;
```

Okay, gehen wir Zeile für Zeile durch

```bash
date=$(date +%Y-%m-%d-%H-%M)
```
Wir legen einen Variabel mit dem Namen `date` fest, die einen String aus dem aktuellen Datum und Uhrzeit beinhaltet.

```bash
export PGPASSWORD=PasswortVomNutzerTest
```
In dieser Zeile legen wir das Passwort vom Nutzer `Test` fest, was als Umgebungsvariable übergeben wird.
Jetzt kommen wir zum eigentlichen Sicherungsbefehl:

```bash
/usr/bin/pg_dump -c -C -f  p /opt/backup/dumps/${date}_postgres_dump.sql --encoding=UTF-8 -U Test
```

* -c enthält im Dump eine Passage, was beim Importieren die bereits vorhandene DB löscht und alles neu anlegt
* -C enthält im Dump eine Passage, mit CREATE TABLE Statments
* -f Speicherort des Dumps (das p bedeutet Plaintext, also SQL)
* --encoding ist, glaube ich klar :-)
* -U Angabe des Datenbankbenutzers, hier `Test`

Und als Letztes ein Befehl der alle Dumps älter als 5 Tage löscht:

```bash
find /opt/backup/dumps -mtime +5 -exec rm {} \;
```

Jetzt legen wir noch den jeweiligen Cronjob an und das war's.

### Alle Datenbanken sichern

Um alle Datenbank zu sichern, gibt es den Befehl `pg_dumpall`:

```bash
pg_dumpall -U postgres > /opt/backup/all.sql
```

Hier wird der Benutzer `postgres` verwendet, was der Default Admin Nutzer ist. Entsprechend sein Passwort muss verwendet werden.

#### Dokumentation
Weitere Hilfe könnt ihr auch in der [Dokumentation](https://www.postgresql.org/docs/current/app-pgdump.html) finden.
