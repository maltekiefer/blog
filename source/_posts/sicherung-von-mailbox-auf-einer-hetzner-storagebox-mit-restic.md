---
title: Sicherung von Mailbox auf einer Hetzner Storagebox mit restic
date: 2020-06-01 14:00:00
tags:
- mailcow
- storagebox
- hetzner
---
Im folgenden Artikel, möchte ich kurz erläutern, wie ihr eine Sicherung von Mailcow auf einer Hetzner Storagebox auslagern könnt.
Ich gehe davon aus, dass ihr bereits eine Hetzner Storagebox habt und den SSH sowie den RSYNC Zugriff erlaubt habt.

## Storagebox vorbereiten

Es werden einige kleine Schritte benötigt, damit ihr mit der Storagebox problemlos arbeiten könnt. Als Erstes generieren wir einen neuen SSH Key:

```bash
ssh-keygen -t ed25519
```

Beantwortet die Fragen nach euren Bedürfnissen, jedoch darf der Key kein Passwort haben. Als Nächstes installieren wir `sshfs` um unsere Storagebox zu mounten:

```bash
apt install sshfs -y
```

Im nächsten Schritt werden wir die Storagebox mounten:

```bash
mkdir /mnt/storagebox
sshfs u123456@u123456.your-storagebox.de:/ /mnt/storagebox/
```

Jetzt noch entsprechend den Ordner und die Datei für den Schlüssel anlegen:

```bash
mkdir /mnt/storagebox/.ssh
touch /mnt/storagebox/.ssh/authorized_keys
```

Jetzt kopieren wir noch den Key auf die Storagebox:

```bash
cat ~/.ssh/id_ed25519.pub >> /mnt/storagebox/.ssh/authorized_keys
```

Als vorletzten Schritt legen wir noch das Sicherungsverzeichnis in der Storagebox an und umounten die Storagebox wieder vom System:

```bash
mkdir /mnt/storagebox/mailserver
fusermount -u /mnt/storagebox
```

Als letzten Schritt, müssen wir noch eine SSH Config Datei anlegen, da die Storagebox einen anderen Port benutzt:

```bash
Host u123456.your-storagebox.de
        User u123456
        Port 23
```

Bitte wieder den Nutzer entsprechend anpassen

##### Danke an Thomas Leister
Diese Schritte sind im groben von Thomas Leistner kopiert und angepasst worden. Danke

## Backup einrichten

Als Erstes legen wir mal die Ordner an die wir brauchen

```bash
mkdir /backup
mkdir /opt/backup
```

Als Nächstes legen wir die Datei `environment.sh` mit folgendem Inhalt an:

```bash
#!/bin/bash
export RESTIC_REPOSITORY="sftp:u1234567.your-storagebox.de:mailserver"
export RESTIC_PASSWORD="MeinSicheresBackupPasswort"
```

Bitte passt entsprechend `u1234567` und `MeinSicheresBackupPasswort` an!

Jetzt legen wir die Datei `backup.files` mit diesem Inhalt an:

```bash
/etc
/backup
/root
/opt
```

Wir ihr dem Namen vielleicht entnehmen könnt, wollen wir, das restic diese Verzeichnisse sichert. Diese könnt ihr natürlich erweitern.
Als Letztes legen wir die Datei `backup.sh` mit folgendem Inhalt an:

```bash
#!/bin/bash

MAILCOW_BACKUP_LOCATION=/backup /opt/mailcow-dockerized/helper-scripts/backup_and_restore.sh backup all --delete-days 30

source /opt/backup/environment.sh

### Backup new stuff
restic backup \
        --verbose \
        --files-from /opt/backup/backup.files \

### Remove old stuff
echo "Deleting old backups ..."
restic forget --prune \
        --keep-last 7 \
        --keep-daily 14 \
        --keep-weekly 4 \
        --keep-monthly 6 \


restic check
restic prune
restic rebuild-index
```
**Hier müsst ihr den Pfad zum Mailcow Verzeichniss anpassen: `/opt/mailcow-dockerized`**
Zum Schluss machen wir das Script jetzt noch ausführbar:

```bash
chmod 700 /opt/backup/backup.sh
```

## Repo initialisieren

Damit restic auch läuft, müsst ihr den folgenden Befehl einmal ausführen, dabei wird die Repo angelegt:

```bash
source /opt/backupt/environment.sh && restic init
```


## Crontab
Jetzt legen wir noch einen Cronjob an, der alle 4 Stunden die Sicherung ausführt:

```bash
0 */4 * * * /bin/bash /opt/backup/backup.sh
```
